import datetime as dt
import requests
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.events import AllSlotsReset
import random

ALLOWED_QTD = ["1","uma","um","2","duas","dois","3","três","tres","4","quatro","5","cinco","6","seis","7","sete","8","oito","9","nove","10","dez"]
ALLOWED_PIZZA_SIZES = ["broto","brotinho","pequena","brot","pequena","pequeno","p","b","peq","medio","médio","media", "média","m","med","méd","grande","grand","g", "família","familia","f","gg","fam","gigante","gig"]
ALLOWED_PIZZA_TYPES = ["4 queijos","quatro queijos","4 queijo","quatro queijo","4-queijo","quatro-queijo","quatro-queijos","4-queijos","camarão","camarao", "calabresa","calab", "atum", "frango","fg","banana","ban","brig","brigadero","brigadeiro","mumu","doce-de-leite","doce de leite"]
ALLOWED_DRINKS = ["refrigerante","refri","coca","coquinha","fanta","sprite","fruki","suco","suquinho","sucão","água","agua","h2o","água de coco","agua de coco","água-de-coco","não","nenhuma","sem"]
ALLOWED_BORDER = ["com borda","sem borda","com","sem"]
ALLOWED_DELIVERY = ["retirar","retirada","pegar","pego","buscar","busco","retiro","retirar","tele-entrega","tele entrega","motoboy","entrega","entregar","delivery","moto"]

entitiy_list = []

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text=f"Se deu essa mensagem é pq deu certo")


class ValidateSimplePizzaForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_sabores"
    
    def validate_tops(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `tops` value."""

        if(type(slot_value)==Text):
            if(slot_value not in ALLOWED_PIZZA_TYPES):
                dispatcher.utter_message(text=f"Nós não fazemos esse sabor!\n Nossos sabores salgados são 4 queijos, camarão, calabresa, atum e frango.\nNossos sabores doces são brigadeiro, banana e doce de leite.\nTemos opções com borda e sem borda!")
                return {"tops": None}
        
        else:
            for i in range(len(slot_value)):
                if(slot_value[i].lower() not in ALLOWED_PIZZA_TYPES):
                    dispatcher.utter_message(text=f"Nós não fazemos esse sabor!\n Nossos sabores salgados são quatro queijos, camarão, calabresa, atum e frango.\nNossos sabores doces são brigadeiro, banana e doce de leite.\nTemos opções com borda e sem borda!")
                    return {"tops": None}
                if(len(slot_value)>=5):
                    dispatcher.utter_message(text=f"Nossos Zzapiolos só conseguem fazer até 4 sabores por pizza! Por favor escolha novamente.")
                    return {"tops": None}

            
        dispatcher.utter_message(text=f" Certo, você quer uma pizza sabor {slot_value} ")
        try:
            for i in range(len(slot_value)):
                entitiy_list.append("tops")

        except:
            entitiy_list.append("tops")
        return {"tops": slot_value}
    
    def validate_size(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `pizza_size` value."""
        if(type(slot_value)==Text):
            if(slot_value.lower() not in ALLOWED_PIZZA_SIZES):
                dispatcher.utter_message(text=f"Ops, não entendi que tamanho é esse.\nNossas pizzas podem ser de tamanho broto, média, grande e família.")
                return {"size": None}
        else:
            if(slot_value[i].lower() not in ALLOWED_PIZZA_SIZES):
                if(slot_value[i].lower() not in ALLOWED_PIZZA_SIZES):
                    dispatcher.utter_message(text=f"Ops, não entendi que tamanhos é esse.\nNossas pizzas podem ser de tamanho broto, média, grande e família.")
                    return {"size": None}


        dispatcher.utter_message(text=f"Certo você quer uma pizza de tamanho {slot_value}.")
        try:
            for i in range(len(slot_value)):
                entitiy_list.append("size")

        except:
            entitiy_list.append("size")

        return {"size": slot_value}
    def validate_qtd(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `qtd` value."""
        if(type(slot_value)==Text):       
            if(slot_value.lower() not in ALLOWED_QTD):
                dispatcher.utter_message(text=f"Não entendi quantas pizzas você quer\nnossos Zzapiolos só conseguem fazer 10 pizzas por pedido\nPor favor, peça novamente!.")
                return {"qtd": None}
        else:
            if(slot_value[i].lower() not in ALLOWED_QTD):
                dispatcher.utter_message(text=f"Não entendi quantas pizzas você quer\nnossos Zzapiolos só conseguem fazer 10 pizzas por pedido\nPor favor, peça novamente!.")
                return {"qtd": None}
    
        dispatcher.utter_message(text=f"Certo! Você quer uma quantidade de  {slot_value} pizzas.")
        try:    
            for i in range(len(slot_value)):
                entitiy_list.append("qtd")

        except:
            entitiy_list.append("qtd")
        
        return {"qtd": slot_value}

    def validate_border(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `border` value."""
        
        if(slot_value.lower() not in ALLOWED_BORDER):
            dispatcher.utter_message(text=f"Não entendi, por favor diga com ou sem borda.")
            return {"border": None}
        
        dispatcher.utter_message(text=f"Certo! Você quer {slot_value}")
        return {"border":slot_value}


     
    
    def validate_drinks(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `drinks` value."""

        if(tracker.get_intent_of_latest_message()=="deny"):
            dispatcher.utter_message(text=f"Certo, vou anotar aqui que você não quer bebida.")
            return {"drinks":"não"}
        elif(tracker.get_intent_of_latest_message()=="affirm"):
            dispatcher.utter_message(text=f"Nós trabalhamos com as seguintes bebidas: Refrigerante, suco, água e água de coco. Qual você gostaria?")
            return {"drinks":"None"}
        
        if(type(slot_value)==Text):
            if(slot_value.lower() not in ALLOWED_DRINKS):
                dispatcher.utter_message(text=f"Ops, não entendi Qual bebida que você quer!\n Nós trabalhamos com as seguintes bebidas: Refrigerante, suco, água e água de coco.")
                return {"drinks": None}

        else:
            for i in range(len(slot_value)):
                if(slot_value[i].lower() not in ALLOWED_DRINKS):
                    dispatcher.utter_message(text=f"Ops, não entendi Qual bebida que você quer!\n Nós trabalhamos com as seguintes bebidas: Refrigerante, suco, água e água de coco.")
                    return {"drinks": None}
        
        dispatcher.utter_message(text=f"Certo você querer para beber: {slot_value}.")
        return {"drinks": slot_value}

    

    def validate_delivery(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `delivery` value."""

        if(slot_value.lower() not in ALLOWED_DELIVERY):
            dispatcher.utter_message(text=f"Ops, não entendi qual o método de entrega!\n Temos opções de Tele-entrega ou retirada na pizzaria.")
            return {"delivery": None}

        dispatcher.utter_message(text=f"Certo o seu método de entrega: {slot_value}.")
        return {"delivery": slot_value}
    
    def validate_tips(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `tips` value."""
        
        if(tracker.get_intent_of_latest_message()=="deny" and slot_value == None):
            dispatcher.utter_message(text=f"Ok =( ")
            return {"tips":0}
       
        try:
            
            
            tips = float(slot_value)
            
        
            
        except:
            print(type(slot_value))
            dispatcher.utter_message(text="Desculpa, não entendi o valor, Poderia digitar  o valor usando algarismos? por exemplo R$ 5.00 ou até mesmo  R$ 12.50 =) ")
            return {"tips":None}
    
        dispatcher.utter_message(text=f"Muito Obrigado, você está dando R$ {tips} de gorjeta")
        return {"tips": f"{tips:.2f}"}

class ActionDefaultFallback(Action):
    def name(self) -> Text:
        return "action_default_fallback"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        stopCounter = tracker.get_slot("stopCounter")

        if(stopCounter== 0):
            #stopCounter+=1
            dispatcher.utter_message(text= f"Desculpe, mas não entendi sua frase.")
            return [SlotSet("stopCounter",1)]
        elif(stopCounter== 1):
            #stopCounter+=1
            dispatcher.utter_message(text= f"Desculpe, mas não entendi sua frase.\nVocê poderia  falar assim por exemplo -> Quero uma pizza de quatro queijos e calabresa com borda e refri!")
            return [SlotSet("stopCounter",2)]
        elif(stopCounter ==2):
            dispatcher.utter_message(text=f"Hmm Não entendi, vou indo nessa! Se quiser pedir uma pizza me chama de novo!(Tenha pena, ainda estou aprendendo)")
            return [AllSlotsReset()]       
      
    









class StopAndReset(Action):

    def name(self) -> Text:
        return "stop_form"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text=f"Certo, se quiser pedir pizza é só me chamar de novo.")

        return[AllSlotsReset()]



class CountOrder(Action):
    def name(self) -> Text:
        return "action_count_order"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        #utils.log(tracker.latest_message)
        # Convertendo qtd em  número

        
        
        qtd_str = tracker.get_slot("qtd")
        size_str = tracker.get_slot("size")
        tops_list = tracker.get_slot("tops")
        drinks_list = tracker.get_slot("drinks")
        border_str = tracker.get_slot("border")
        delivery_str = tracker.get_slot("delivery")
        tips = tracker.get_slot("tips")
        tips = float(tips)
            
        qtd_dict = {"1":1,"uma":1,"um":1,"2":2,"duas":2,"dois":2,"3":3,"três":3,"tres":3,"4":4,"quatro":4,"5":5,"cinco":5,"6":6,"seis":6,"7":7,"sete":7,"8":8,"oito":8,"9":9,"nove":9,"10":10,"dez":10}
        size_dict = {"broto":10,"brotinho":10,"pequena":10,"brot":10,"pequena":10,"pequeno":10,"p":10,"b":10,"peq":10,"medio":15,"médio":15,"media":15, "média":15,"m":15,"med":15,"méd":15,"grande":20,"grand":20,"g":20,"família":25,"familia":25,"f":25,"gg":25,"fam":25,"gigante":25,"gig":25}
        tops_dict = {"4 queijos":10,"quatro queijos":10,"4 queijo":10,"quatro queijo":10,"4-queijo":10,"quatro-queijo":10,"quatro-queijos":10,"4-queijos":10,"camarão":10,"camarao":10, "calabresa":10,"calab":10, "atum":10, "frango":10,"fg":10,"banana":20,"ban":20,"brig":20,"brigadero":20,"brigadeiro":20,"mumu":20,"doce-de-leite":20,"doce de leite":20}
        drinks_dict = {"refrigerante":5,"refri":5,"coca":5,"coquinha":5,"fanta":5,"sprite":5,"fruki":5,"suco":5,"suquinho":5,"sucão":5,"água":3,"agua":3,"h2o":3,"água de coco":8,"agua de coco":8,"água-de-coco":8,"não":0,"nenhuma":0,"sem":0}
        border_dict = {"com borda":10,"sem borda":0,"com":10,"sem":10}
        delivery_dict = {"retirar":0,"retirada":0,"pegar":0,"pego":0,"buscar":0,"busco":0,"retiro":0,"retirar":0,"tele-entrega":10,"tele entrega":10,"motoboy":10,"entrega":10,"entregar":10,"delivery":10,"moto":10}

        if qtd_str.lower() in qtd_dict:
            index = list(qtd_dict).index(qtd_str.lower())
            list_aux = list(qtd_dict.values())
            qtd = list_aux[index]
            
        if size_str.lower() in size_dict:
            index = list(size_dict).index(size_str.lower())
            list_aux = list(size_dict.values())
            size = list_aux[index]

        tops = 0
        if(type(tops_list)== Text):
            if tops_list.lower() in tops_dict:
                index = list(tops_dict).index(tops_list.lower())
                list_aux = list(tops_dict.values())
                tops = list_aux[index]
        else:
            for i in range(len(tops_list)):
                aux_str = tops_list[i].lower()
                if aux_str in tops_dict:
                    index = list(tops_dict).index(aux_str)
                    list_aux  = list(tops_dict.values())
                    tops += list_aux[index] 

            

        drinks = 0
        if(type(drinks_list)==Text):
            if drinks_list in drinks_dict:
                index = list(drinks_dict).index(drinks_list.lower())
                list_aux = list(drinks_dict.values())
                drinks = list_aux[index]
        else:
            for i in range(len(drinks_list)):
                aux_str = drinks_list[i].lower()
                if aux_str in drinks_dict:
                    index = list(drinks_dict).index(aux_str)
                    list_aux  = list(drinks_dict.values())
                    drinks += list_aux[index] 
                
        if border_str.lower() in border_dict:
            index = list(border_dict).index(border_str.lower())
            list_aux = list(border_dict.values())
            border = list_aux[index]


        if delivery_str.lower() in delivery_dict:
            index = list(delivery_dict).index(delivery_str.lower())
            list_aux = list(delivery_dict.values())
            delivery = list_aux[index]

        bill = qtd*(size + tops + border) + drinks + delivery + tips

        dispatcher.utter_message(text=f" Aqui está o seu Checkout:\nVocê pediu {qtd_str} pizza(s)\nTamanho(s): {size_str}\nSabor(es): {tops_list}\nOpção de borda: {border_str}\n Opção de bebida(s): {drinks_list}\nOpção de entrega: {delivery_str}\nValor da Gorjeta: R${tips}\nSeu pedido ficou R${bill} reais.\nPosso Confirmar seu pedido?")
        return [SlotSet("doneOrder",True)]
















class ActionCheckHours(Action):
    def name(self) -> Text:
        return "check_hours_greet"
    
    def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        now = dt.datetime.now()
        today0600 = now.replace(hour=6, minute=0, second=0, microsecond=0)
        today1159 = now.replace(hour=11, minute=59, second=59, microsecond=0)
        today1200 = now.replace(hour=12, minute=0, second=0, microsecond=0)
        today1759 = now.replace(hour=17, minute=59, second=59, microsecond=0)
        today1800 = now.replace(hour=18, minute=00, second=00, microsecond=0)
        today0559 = now.replace(hour=5, minute=59, second=59, microsecond=0)
       
       # doneOrder = False 
        
        
        if(now >= today0600 and now <= today1159 ):
            greet = "Bom Dia"
        elif(now >= today1200 and now <= today1759 ):

            greet = "Boa Tarde"
        
        elif(now >= today1800 or now <=today0559):
            greet = "Boa Noite" 

       
        firstContact = tracker.get_slot("first")
        doneOrder = tracker.get_slot("doneOrder")  # slot bool inicia como False e após form completo sem fail então doneOrder = True

        if(firstContact == False and doneOrder == False):
            dispatcher.utter_message(text=f"{greet}! Eu sou Zzapi o Atendente Robótico!\n Aqui na Zzapi Pizzaria nós trabalhamos com pizzas doces e salgadas!\n Gostaria de pedir uma pizza?")
            

        elif(firstContact == True and doneOrder == False ):
            dispatcher.utter_message(text=f"{greet}! Agradeço o seu retorno!\n Gostaria que eu anotasse seu pedido?")
        
        elif(firstContact == False and doneOrder == True ):
           dispatcher.utter_message(text=f"{greet}! Nós já estamos aprontando seu pedido!\n Ele ficará pronto daqui uns 20-30 minutos.")
        
        elif(firstContact == True and doneOrder == True ):
           dispatcher.utter_message(text=f"{greet}! Obrigado por nos contatar novamente.\nSeu pedido está  quase pronto!\n Eu te mando mensagem quando terminar aqui!.")
        
            
    
        return [SlotSet("first",True)]